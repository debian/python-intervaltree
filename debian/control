Source: python-intervaltree
Section: python
Priority: optional
Maintainer: Hilko Bengen <bengen@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 python3-all,
 python3-setuptools,
 python3-sortedcontainers,
 python3-pytest,
Rules-Requires-Root: no
Standards-Version: 4.3.0
Homepage: https://pypi.python.org/pypi/intervaltree
Vcs-Git: https://salsa.debian.org/debian/python-intervaltree.git
Vcs-Browser: https://salsa.debian.org/debian/python-intervaltree

Package: python3-intervaltree
Architecture: all
Multi-Arch: foreign
Depends: ${python3:Depends}, ${misc:Depends}
Description: mutable, self-balancing interval tree (Python 3)
 This library contains a mutable, self-balancing interval tree
 implementation for Python. Queries may be by point, by range overlap,
 or by range envelopment.
 .
 It was designed to allow tagging text and time intervals, where the
 intervals include the lower bound but not the upper bound.
 .
 This package installs the library for Python 3.
